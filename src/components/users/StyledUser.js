import { Link } from 'react-router-dom'

import styled from 'styled-components'

import { device } from '../../styles/Devices'

export const Container = styled.section`
  padding-right: 1.5rem;
  padding-left: 1.5rem;
  margin-left: auto;
  margin-right: auto;
  width: 100%;

  @media ${device.laptopL} {
    max-width: 114rem;
  }
  @media ${device.laptop} {
    max-width: 96rem;
  }
  @media ${device.tablet} {
    max-width: 72rem;
  }
  @media ${device.mobileL} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileM} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileS} {
    padding-left: 1.2rem;
    padding-right: 1.2rem;
  }
`

export const BackButton = styled(Link)`
  padding: 0.7rem 1.2rem;
  font-size: 1.8rem;
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgb(74, 142, 255);
  color: white;
  margin: 2rem;
  border-radius: 50rem;
  z-index: 10;
  box-shadow: 0px 25px 50px -12px rgb(74, 142, 255);
`

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 7rem;

  @media ${device.tablet} {
    flex-direction: column;
    margin-top: 13rem;
  }
  @media ${device.mobileL} {
    flex-direction: column;
    margin-top: 13rem;
  }
  @media ${device.mobileM} {
    flex-direction: column;
    margin-top: 13rem;
  }
  @media ${device.mobileS} {
    flex-direction: column;
    margin-top: 13rem;
  }
`

export const Centered = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
`

// export const CenteredFlex = styled.div`
//   display: flex;
//   justify-content: center;
//   align-items: center;
// `

export const CenteredBetween = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  div:first-child {
    margin-right: 2rem;
  }

  @media ${device.mobileL} {
    flex-direction: column;
  }
  @media ${device.mobileM} {
    flex-direction: column;
  }
  @media ${device.mobileS} {
    flex-direction: column;
  }
`

export const ProfileCard = styled.article`
  padding: 2rem;
  background-color: white;
  color: black;
  box-shadow: 12px 12px 40px 0 rgba(0, 0, 0, 0.12);
  border-radius: 0.5rem;
  transition: all 0.2s ease;
  margin: 5rem auto;
  min-width: 0;
  width: 100%;
  max-width: 70rem;
  position: relative;
  height: 100%;

  &:hover {
    box-shadow: 0 20px 25px -5px rgba(0, 0, 0, 0.1),
      0 10px 10px -5px rgba(0, 0, 0, 0.04);
  }

  @media ${device.tablet} {
    margin: 0 auto;
    margin-bottom: 4rem;
    max-width: 50rem;
  }
  @media ${device.mobileL} {
    margin: 0 auto;
    margin-bottom: 4rem;
  }
  @media ${device.mobileM} {
    margin: 0 auto;
    margin-bottom: 4rem;
  }
  @media ${device.mobileS} {
    margin: 0 auto;
    margin-bottom: 4rem;
  }
`

export const ProfileImg = styled.div`
  position: absolute;
  top: -8rem;
  img {
    border-radius: 50%;
    height: 15rem;
    width: 15rem;
    box-shadow: 0px 5px 50px 0px rgb(53, 150, 201),
      0px 0px 0px 7px rgba(74, 142, 255, 0.5);
  }
`

export const Name = styled.div`
  font-size: 2rem;
  font-weight: 600;
  margin-top: 8rem;
  margin-bottom: 1rem;
  padding: 2rem 0 0 0;
  color: rgb(74, 142, 255);
  font-weight: bold;
`

export const Username = styled.span`
  font-size: 1.6rem;
  font-weight: 400;
  color: rgb(53, 150, 201);

  a {
    &:visited {
      color: rgb(53, 150, 201);
    }
  }
`

export const Contact = styled.div`
  font-size: 1.7rem;
  font-weight: 600;
  display: flex;
  align-items: center;
  text-transform: ${props => (props.capital ? 'capitalize' : 'none')};
  margin: 0.5rem 0;

  span:first-child {
    margin-right: 0.5rem;
  }
`

export const Line = styled.div`
  margin: 1.8rem 0;
  border-bottom: 1px solid #ebedf0;
`

export const Bio = styled.div`
  max-width: 80%;
  margin: 0 auto;

  p {
    font-size: 1.8rem;
    font-weight: 400;
    color: #484848;
    text-align: center;
  }

  @media ${device.mobileL} {
    max-width: 100%;
  }
  @media ${device.mobileM} {
    max-width: 100%;
  }
  @media ${device.mobileS} {
    max-width: 100%;
  }
`

export const Populer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(10rem, 1fr));
  grid-gap: 1rem;
  grid-auto-rows: minmax(7rem, 1fr);
  justify-content: space-around;
  align-items: center;

  @media ${device.mobileL} {
    grid-template-columns: repeat(auto-fit, minmax(15rem, 1fr));
  }
  @media ${device.mobileM} {
    grid-template-columns: repeat(auto-fit, minmax(10rem, 1fr));
  }
  @media ${device.mobileS} {
    grid-template-columns: repeat(auto-fit, minmax(10rem, 1fr));
  }
`

export const SeparateReverse = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 1rem;

  p:first-child {
    font-size: 2.4rem;
    font-weight: 700;
    color: #465161;
    line-height: 1.2;
    letter-spacing: 1px;
    margin-bottom: 0.5rem;
  }

  p:last-child {
    text-transform: lowercase;
    font-size: 1.7rem;
    font-weight: 500;
    color: #57595d;
  }
`

export const RoundedButton = styled.a`
  padding: 1.8rem 4.7rem;
  font-size: 2rem;
  margin: 1.5rem 0;
  border-radius: 5rem;
  border: 1px solid transparent;
  background-color: rgb(44, 130, 79);
  color: white;
  text-align: center;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  transition: all 0.2s ease;

  &:hover {
    box-shadow: 0px 25px 50px -12px rgb(44, 130, 79);
    transform: translateY(-1px);
  }
`

export const ReposAll = styled.section`
  margin: 4rem 0;

  h4 {
    margin-bottom: 2rem;
  }
`
export const ReposContainer = styled.section`
  display: grid;
  grid-gap: 2rem;
  grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
  grid-auto-rows: minmax(10rem, 1fr);
`

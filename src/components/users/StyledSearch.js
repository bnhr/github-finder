import styled from 'styled-components'

import { device } from '../../styles/Devices'

export const All = styled.section`
  height: 100%;
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: linear-gradient(
    to right top,
    #4274d3,
    #2c81d3,
    #258ccf,
    #3596c9,
    #4c9ec2
  );

  form {
    margin-bottom: 5vh;
  }
`

export const XButton = styled.button`
  border: 1px solid transparent;
  background-color: transparent;
  cursor: pointer;
  font-size: 2rem;
  transition: all 0.2s ease;

  svg {
    display: flex;
    align-items: center;
    justify-content: center;

    @media ${device.mobileL} {
      height: 2rem;
      width: 2rem;
    }
    @media ${device.mobileM} {
      height: 2rem;
      width: 2rem;
    }
    @media ${device.mobileS} {
      height: 2rem;
      width: 2rem;
    }
  }
`

export const Container = styled.section`
  padding-right: 1.5rem;
  padding-left: 1.5rem;
  margin-left: auto;
  margin-right: auto;
  width: 100%;

  @media ${device.laptopL} {
    max-width: 100rem;
  }
  @media ${device.laptop} {
    max-width: 96rem;
  }
  @media ${device.tablet} {
    max-width: 72rem;
  }
  @media ${device.mobileL} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileM} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileS} {
    padding-left: 1.2rem;
    padding-right: 1.2rem;
  }
`

export const Heading = styled.h1`
  text-align: center;
  color: white;
  font-size: calc(4rem + (68 - 40) * ((100vw - 30rem) / (1600 - 300)));
`

export const SearchContainer = styled.div`
  display: flex;
  position: relative;
  border: 1px solid transparent;
  border-radius: 7.5rem;
  display: flex;
  align-items: center;
  background-color: white;
  margin-bottom: 2rem;
  padding: 0.7rem 2rem;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  transition: all 0.2s ease;
`

export const Icon = styled.div`
  display: flex;
  align-items: center;
  padding-right: 1.2rem;

  svg {
    color: gray;
  }

  @media ${device.tablet} {
    svg {
      width: 25px;
      height: 25px;
    }
  }
  @media ${device.mobileL} {
    svg {
      width: 20px;
      height: 20px;
    }
  }
  @media ${device.mobileM} {
    svg {
      width: 20px;
      height: 20px;
    }
  }
  @media ${device.mobileS} {
    svg {
      width: 20px;
      height: 20px;
    }
  }
`

export const SearchStyle = styled.input`
  position: relative;
  border: 1px solid transparent;
  font-size: calc(1.8rem + (40 - 18) * ((100vw - 30rem) / (1600 - 300)));
  color: #4579b9;
  border-radius: 6px;
  text-align: center;
  text-transform: lowercase;
  display: block;
  margin: calc(1rem + (20 - 10) * ((100vw - 30rem) / (1600 - 300))) 0;
  width: 100%;
  min-width: 50rem;
  background-color: transparent;

  &:focus {
    outline: 0;
  }

  @media ${device.tablet} {
    min-width: 0;
  }
  @media ${device.mobileL} {
    min-width: 0;
  }
  @media ${device.mobileM} {
    min-width: 0;
  }
  @media ${device.mobileS} {
    min-width: 0;
  }
`

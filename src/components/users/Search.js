import React, { useState, useContext } from 'react'
import GithubContext from '../../context/github/githubContext'
import AlertContext from '../../context/alert/alertContext'

import * as StyledSearch from './StyledSearch'

const Search = () => {
  const githubContext = useContext(GithubContext)
  const alertContext = useContext(AlertContext)

  const [text, setText] = useState('')

  const onSubmit = e => {
    e.preventDefault()
    if (text === '') {
      alertContext.setAlert('Please enter something!')
    } else {
      githubContext.searchUsers(text)
      setText('')
    }
  }

  const onChange = e => setText(e.target.value)

  return (
    <form onSubmit={onSubmit}>
      <StyledSearch.Heading>Octofind</StyledSearch.Heading>
      <StyledSearch.SearchContainer>
        <StyledSearch.Icon>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            width='40'
            height='40'
            viewBox='0 0 24 24'
            fill='none'
            stroke='currentColor'
            strokeWidth='2'
            strokeLinecap='round'
            strokeLinejoin='round'
            className='feather feather-search'>
            <circle cx='11' cy='11' r='8'></circle>
            <line x1='21' y1='21' x2='16.65' y2='16.65'></line>
          </svg>
        </StyledSearch.Icon>

        <StyledSearch.SearchStyle
          type='search'
          name='text'
          id=''
          placeholder='Search User...'
          value={text}
          onChange={onChange}
        />
        {githubContext.users.length > 0 && (
          <StyledSearch.XButton onClick={githubContext.clearUsers}>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='40'
              height='40'
              viewBox='0 0 24 24'
              fill='none'
              stroke='red'
              strokeWidth='2'
              strokeLinecap='round'
              strokeLinejoin='round'
              className='feather feather-x-circle'>
              <circle cx='12' cy='12' r='10'></circle>
              <line x1='15' y1='9' x2='9' y2='15'></line>
              <line x1='9' y1='9' x2='15' y2='15'></line>
            </svg>
          </StyledSearch.XButton>
        )}
      </StyledSearch.SearchContainer>
    </form>
  )
}

export default Search

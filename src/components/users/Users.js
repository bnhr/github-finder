import React, { useContext } from 'react'
import UserItem from './UserItem'
import Spinner from '../layout/Spinner'
import GithubContext from '../../context/github/githubContext'
import styled from 'styled-components'

import { device } from '../../styles/Devices'

const UsersItem = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(12rem, 1fr));
  grid-gap: 2rem;
  grid-auto-rows: minmax(10rem, 1fr);

  @media ${device.mobileL} {
    margin-bottom: 2rem;
  }
  @media ${device.mobileM} {
    margin-bottom: 2rem;
  }
  @media ${device.mobileS} {
    margin-bottom: 2rem;
  }
`

const Users = () => {
  const githubContext = useContext(GithubContext)

  const { loading, users } = githubContext

  if (loading) {
    return <Spinner />
  } else {
    return (
      <UsersItem>
        {users.map(user => (
          <UserItem key={user.id} user={user} />
        ))}
      </UsersItem>
    )
  }
}

export default Users

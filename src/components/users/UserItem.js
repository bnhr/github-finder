import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

const UserItems = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: white;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  border-radius: 1rem;

  img {
    max-width: 4rem;
    margin-bottom: 0.4rem;
  }
`

const StyledLink = styled(Link)`
  color: black;
`

const UserItem = ({ user: { login, avatar_url } }) => {
  return (
    <UserItems className='card text-center'>
      <StyledLink to={`/user/${login}`}>
        <img src={avatar_url} alt='Avatar URL' />
      </StyledLink>
      <div>
        <StyledLink to={`/user/${login}`}>{login}</StyledLink>
      </div>
    </UserItems>
  )
}

UserItem.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserItem

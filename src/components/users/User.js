import React, { Fragment, useEffect, useContext } from 'react'
import Spinner from '../layout/Spinner'
import Repos from '../repos/Repos'
import GithubContext from '../../context/github/githubContext'

import * as StyledUser from './StyledUser'

const User = ({ match }) => {
  const githubContext = useContext(GithubContext)

  const { getUser, loading, user, repos, getUserRepos } = githubContext

  useEffect(() => {
    getUser(match.params.login)
    getUserRepos(match.params.login)
    // eslint-disable-next-line
  }, [])

  const {
    name,
    avatar_url,
    location,
    bio,
    email,
    blog,
    login,
    html_url,
    followers,
    following,
    public_repos,
    public_gists,
    hireable
  } = user

  if (loading) {
    return <Spinner />
  }
  return (
    <Fragment>
      {/* Container */}
      <StyledUser.Container>
        {/* Back button */}
        <StyledUser.BackButton to='/'>
          {' '}
          &larr; Back to search
        </StyledUser.BackButton>
        {/* Wrapper */}
        <StyledUser.Wrapper>
          {/* ProfilCard */}
          <StyledUser.ProfileCard>
            <StyledUser.Centered>
              <StyledUser.ProfileImg>
                <img src={avatar_url} alt='Avatar' />
              </StyledUser.ProfileImg>
              <StyledUser.Name>
                {name} -{' '}
                <StyledUser.Username>
                  <a href={html_url}>@{login}</a>
                </StyledUser.Username>
              </StyledUser.Name>
              {location && (
                <Fragment>
                  <StyledUser.Contact capital>
                    <span>
                      <svg
                        xmlns='http://www.w3.org/2000/svg'
                        width='16'
                        height='16'
                        viewBox='0 0 24 24'
                        fill='none'
                        stroke='currentColor'
                        strokeWidth='2'
                        strokeLinecap='round'
                        strokeLinejoin='round'
                        className='feather feather-map-pin'>
                        <path d='M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z'></path>
                        <circle cx='12' cy='10' r='3'></circle>
                      </svg>
                    </span>
                    <span>{location}</span>
                  </StyledUser.Contact>
                </Fragment>
              )}
              <StyledUser.CenteredBetween>
                {blog && (
                  <Fragment>
                    <StyledUser.Contact>
                      <span>
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          width='24'
                          height='24'
                          viewBox='0 0 24 24'
                          fill='none'
                          stroke='currentColor'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                          className='feather feather-globe'>
                          <circle cx='12' cy='12' r='10'></circle>
                          <line x1='2' y1='12' x2='22' y2='12'></line>
                          <path d='M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z'></path>
                        </svg>
                      </span>
                      <span>{blog}</span>
                    </StyledUser.Contact>
                  </Fragment>
                )}
                {email && (
                  <Fragment>
                    <StyledUser.Contact>
                      <span>
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          width='24'
                          height='24'
                          viewBox='0 0 24 24'
                          fill='none'
                          stroke='currentColor'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                          className='feather feather-mail'>
                          <path d='M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z'></path>
                          <polyline points='22,6 12,13 2,6'></polyline>
                        </svg>
                      </span>
                      <span>{email}</span>
                    </StyledUser.Contact>
                  </Fragment>
                )}
              </StyledUser.CenteredBetween>
            </StyledUser.Centered>
            {bio && (
              <Fragment>
                <StyledUser.Line />
                <StyledUser.Bio>
                  <p>{bio}</p>
                </StyledUser.Bio>
              </Fragment>
            )}
            <StyledUser.Line />
            <StyledUser.Populer>
              <StyledUser.SeparateReverse>
                <p>{followers}</p>
                <p>followers</p>
              </StyledUser.SeparateReverse>
              <StyledUser.SeparateReverse>
                <p>{following}</p>
                <p>following</p>
              </StyledUser.SeparateReverse>
              <StyledUser.SeparateReverse>
                <p>{public_repos}</p>
                <p>public repos</p>
              </StyledUser.SeparateReverse>
              <StyledUser.SeparateReverse>
                <p>{public_gists}</p>
                <p>public gists</p>
              </StyledUser.SeparateReverse>
            </StyledUser.Populer>
            {hireable ? (
              <Fragment>
                <StyledUser.Centered>
                  <StyledUser.RoundedButton href={html_url}>
                    Hire me now
                  </StyledUser.RoundedButton>
                </StyledUser.Centered>
              </Fragment>
            ) : (
              <Fragment>
                <StyledUser.Centered>
                  <StyledUser.RoundedButton href={html_url}>
                    Check my github
                  </StyledUser.RoundedButton>
                </StyledUser.Centered>
              </Fragment>
            )}
          </StyledUser.ProfileCard>
          <StyledUser.ReposAll>
            <h4>Recent repos :</h4>
            <StyledUser.ReposContainer>
              <Repos repos={repos} />
            </StyledUser.ReposContainer>
          </StyledUser.ReposAll>
        </StyledUser.Wrapper>
      </StyledUser.Container>
    </Fragment>
  )
}

export default User

import styled, { keyframes } from 'styled-components'

export const fadein = keyframes`
  from {top: 0; opacity: 0;} 
  to {top: 30px; opacity: 1;}
`

export const fadeout = keyframes`
  from {top: 30px; opacity: 1;}
  to {top: 0; opacity: 0;}
`

export const Toast = styled.div`
  background-color: #fff;
  color: #3596c9;
  font-size: 2rem;
  padding: 3rem;
  position: absolute;
  top: 1.2rem;
  right: 1.2rem;
  border-radius: 5px;
  text-align: center;
  z-index: 10;
`

import React, { useContext } from 'react'
import AlertContext from '../../context/alert/alertContext'

import * as StyledAler from './StyledAlert'

const Alert = () => {
  const alertContext = useContext(AlertContext)
  const { alert } = alertContext
  return (
    alert !== null && (
      <StyledAler.Toast>
        {' '}
        <span role='img' aria-label='please'>
          😫
        </span>{' '}
        {alert.msg}
      </StyledAler.Toast>
    )
  )
}

export default Alert

import React from 'react'
import loading from './loading.svg'

import styled from 'styled-components'
import { Container } from '../users/StyledSearch'

const Loading = styled.div`
  margin: 0 auto;
  width: 100%;
  height: auto;
  min-width: 10rem;
`

const Spinner = () => {
  return (
    <Container>
      <Loading>
        <img src={loading} alt='Loading..' />
      </Loading>
    </Container>
  )
}

export default Spinner

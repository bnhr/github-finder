import React, { Fragment } from 'react'
import Search from '../users/Search'
import Users from '../users/Users'

import { All, Container } from '../users/StyledSearch'

const Home = () => (
  <Fragment>
    <All>
      <Container>
        <Search />
        <Users />
      </Container>
    </All>
  </Fragment>
)

export default Home

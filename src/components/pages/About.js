import React, { Fragment } from 'react';

const About = () => {
  return (
    <Fragment>
      <h1>About this app</h1>
      <p>App to search Github User</p>
    </Fragment>
  );
};

export default About;

import React from 'react'

const NotFound = () => {
  return (
    <>
      <h1>Not found</h1>
      <p className='lead'>Nothing showing here, go back</p>
    </>
  )
}

export default NotFound

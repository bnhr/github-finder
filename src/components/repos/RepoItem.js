import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import * as StyledRepo from './RepoStyle'

const RepoItem = ({ repo }) => {
  return (
    <StyledRepo.ReposItem>
      <StyledRepo.All>
        <h3>
          <a href={repo.html_url}>{repo.name}</a>
        </h3>
        <StyledRepo.Desc>
          {repo.description ? (
            <Fragment>{repo.description}</Fragment>
          ) : (
            <Fragment>Just {repo.name} project nothing more.</Fragment>
          )}
        </StyledRepo.Desc>
      </StyledRepo.All>
      <StyledRepo.MisWrap>
        <div>
          <span>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='16'
              height='16'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              strokeWidth='2'
              strokeLinecap='round'
              strokeLinejoin='round'
              className='feather feather-file'>
              <path d='M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z'></path>
              <polyline points='13 2 13 9 20 9'></polyline>
            </svg>
          </span>
          {repo.language ? (
            <Fragment> {repo.language}</Fragment>
          ) : (
            <Fragment> Can not detect</Fragment>
          )}
        </div>
        <div>{repo.size} KB</div>
      </StyledRepo.MisWrap>
    </StyledRepo.ReposItem>
  )
}

RepoItem.propTypes = {
  repo: PropTypes.object.isRequired
}

export default RepoItem

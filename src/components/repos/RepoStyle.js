import styled from 'styled-components'

export const ReposContainer = styled.section`
  display: grid;
  grid-gap: 2rem;
  grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
  grid-auto-rows: minmax(10rem, 1fr);
`

export const All = styled.div`
  max-width: 100%;
  flex: 1 0 auto;
`

export const ReposItem = styled.div`
  padding: 2rem 2.2rem;
  background-color: white;
  border-radius: 0.5rem;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.2),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  margin-bottom: 1.3rem;
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;

  h3 {
    a {
      font-size: 2.2rem;
      font-weight: 600;
      margin-bottom: 1rem;
      color: rgb(74, 142, 255);
      font-weight: bold;
    }
  }
`

export const Desc = styled.div`
  margin-top: 1.2rem;
  margin-bottom: 2rem;
`

export const MisWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-shrink: 0;
`
